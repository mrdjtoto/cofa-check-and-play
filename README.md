# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

TWe need to configure Kurento to be able to receive the video flow from the survelaince platform and send the flow to the client via WebRTC in case of there will be an activity and it will be considered valid after watermark validator.

Kurento will be managed by a **node.js** server that will allows reproducing video when both of below cases will happen:
* The watermark in the video will be correct
* A JSON message from Data Fusion (FCS) will notify a *new activity event*
* 

### How do I get set up? ###

To setup the application you need to install Kurento Media Server and node.js. It's enough to type "npm start" to execute a http-server client side and watch the demo at [http://localhost:8080](http://localhost:8080).
To let the video play you need to open another windows (or tab, without close the main page) in the same browser and go to the address [http://localhost:8080/checker.html](http://localhost:8080/checker.html). After you have established a connection from the main page and pressed play from it, you can press  "Valid" button from checker.html page. "Unvalid" button is useless now, and it will be used for future features.

It's highly advice to open before main page and after checker page.

If you have some troubles to execute the demo I recommend you to delete the "node_modules" folder and type in terminal "npm install".

This applications contain the logo of COFA that owns to theirself and it's used only for demo purpose.
