var ws = new WebSocket('ws://' + location.host + '/cofa-check-and-play');

var videoElement;
//var videoOutput;
var webRtc;
var state = null;

var PeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
var IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
var SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;

const I_CAN_START = 0;
const I_CAN_STOP = 1;
const I_AM_STARTING = 2;
const I_AM_PAUSE = 3;
var pauseStatus = 0;


window.onload = function() {
    console.log("Page loaded ...");
    console = new Console('console', console);
    videoElement = document.getElementById('videoElement');
    setState(I_CAN_START);
}

window.onbeforeunload = function() { ws.close(); }

ws.onmessage = function(message) {
    var parsedMessage = JSON.parse(message.data);
    console.info('Received message: ' + message.data);

    switch (parsedMessage.id) {
        case 'startResponse':
            startResponse(parsedMessage);
            break;

        case 'error':
            if (state == I_AM_STARTING) {
                setState(I_CAN_START);
            }
            onError("Error message from server: " + parsedMessage.message);
            break;

        case 'waiting':
            console.warn("Waiting for WM check and event trigger.");
            var outputPElement = document.getElementById("waitingMessageArea");
            outputPElement.innerHTML = '<div class="alert alert-info" role="alert"> <a href="checker.html" target="_blank">open checker.html page.</a> </div>'
            break;

        case 'Intrusion detection':
                console.warn('Threat ID: ' + parsedMessage.threat_id);
                console.warn('ID: ' + parsedMessage.id);
                console.warn('Score: ' + parsedMessage.score);
                console.warn('Position X: ' + parsedMessage.position.x + '  Position Y: ' + parsedMessage.position.y);
                console.warn('Timestamp: ' + parsedMessage.timestamp);
                var outputPElement = document.getElementById("waitingMessageArea");
                outputPElement.innerHTML = '<div class="alert alert-success" role="alert">Event detected!</div>';
                break;

        default:
            if (state == I_AM_STARTING) {
                setState(I_CAN_START);
            }
            onError('Unrecognized message', parsedMessage);
    }
}

function playButton() {
    if (pauseStatus == 0) start();
    else playFunction();
}

function start() {
    console.log("DEBUG: Starting streaming...");
    // Disable start button
    setState(I_AM_STARTING);
    console.log("DEBUG: Creating WebRtcPeer and generating local sdp offer ...");
    console.log('DEBUG: Start "startReceiveOnly"...')
    webRtc = kurentoUtils.WebRtcPeer.startRecvOnly(videoElement, onOffer, onError);
    console.log('DEBUG: "startReceiveOnly created.')
}

function playFunction() {
    if (pauseStatus == 1) pauseStatus = 0;
    var message = {
        id: 'replay'
    }
    sendMessage(message);
}

function onOffer(offerSdp) {
    console.info('Invoking SDP offer callback function ' + location.host);

    var message = {             //message to Node.js
            id: 'start',
            sdpOffer: offerSdp
        }               
    sendMessage(message);       //Sending message to Node.js
}

function onError(error) {
    console.error(error);
}

function startResponse(message) {
    setState(I_CAN_STOP);
    console.log("SDP answer received from server. Processing ...");
    webRtc.processSdpAnswer(message.sdpAnswer);
}

function stop() {
    console.log("Stopping video call ...");
    setState(I_CAN_START);
    if (webRtc) {
        webRtc.dispose();
        webRtc = null;
        var message = {
            id: 'stop'
        }
        sendMessage(message);
    }
}

function pauseButton() {
    console.log("Pausing streaming...");
    setState(I_AM_PAUSE);
    pauseStatus = 1;
    var message = {
        id: 'pause'
    }
    sendMessage(message);
}


function setState(nextState) {
    switch (nextState) {

        case I_CAN_START:
            $('#start').attr('disabled', false);
            $('#stop').attr('disabled', true);
            $('#pause').attr('disabled', true);
            break;

        case I_AM_PAUSE:
            $('#start').attr('disabled', false);
            $('#stop').attr('disabled', false);
            $('#pause').attr('disabled', false);
            break;

        case I_CAN_STOP:
            $('#start').attr('disabled', true);
            $('#stop').attr('disabled', false);
            $('#pause').attr('disabled', false);
            break;

        case I_AM_STARTING:
            $('#start').attr('disabled', true);
            $('#stop').attr('disabled', true);
            $('#pause').attr('disabled', true);
            break;

        default:
            onError("Unknown state " + nextState);
            return;
    }
    state = nextState;
}

function sendMessage(message) {
    var jsonMessage = JSON.stringify(message);
    console.log('Senging message: ' + jsonMessage);
    ws.send(jsonMessage);
}


/* Lightbox utility (to display media pipeline image in a modal dialog */
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});