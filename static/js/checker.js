var ws = new WebSocket('ws://' + location.host + '/cofa-check-and-play');

window.onload = function() {
    console.log("Page 'checker' loaded ...");
    console = new Console('console', console);
}

window.onbeforeunload = function() { ws.close(); }

ws.onmessage = function(message) {
    var parsedMessage = JSON.parse(message.data);
    console.info('Received message: ' + message.data);

    switch (parsedMessage.id) {
        case 'startResponse':
            startResponse(parsedMessage);
            break;

        case 'error':
            onError("Error message from server: " + parsedMessage.message);
            break;

        case 'Intrusion detection':
                console.warn('Confirmed Intrusion Detection from server');
                break;

        default:
            onError('Unrecognized message', parsedMessage);
    }
}

function Event() {
    console.log("DEBUG: Event triggered.");
        /*var message = {
            id: 'Event',
        } //message to Node.js*/

        var message = {
            threat_id: 1,
            id: 'Intrusion detection',
            score: 3,
            position : { x : 300, y : 200, },
            timestamp: '0x00222',
        }

    sendMessage(message);
}

function endEvent() {
    console.log("DEBUG: Event triggered.");
        var message = {
            id: 'endEvent',
        } //message to Node.js
    sendMessage(message);
}

function sendMessage(message) {
    var jsonMessage = JSON.stringify(message);
    console.log('Senging message: ' + jsonMessage);
    ws.send(jsonMessage);
}

function onError(error) {
    console.error(error);
}