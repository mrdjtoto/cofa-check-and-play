var path = require('path');
var express = require('express');
var session = require('express-session')
var ws = require('ws');
var minimist = require('minimist');
var url = require('url');
var kurento = require('kurento-client');

var argv = minimist(process.argv.slice(2), {
    default: {
        as_uri: "http://localhost:8080/",
        ws_uri: "ws://localhost:8888/kurento",
        //file_uri: 'file:///home/antonio/Desktop/videoh264.MP4'
        //file_uri:'rtp://127.0.0.1:5004/'
        //file_uri: 'rtsp://10.1.2.232:8555/'
        file_uri: 'http://files.kurento.org/video/puerta-del-sol.ts'
    }
});

var app = express();

/* Management of sessions */
app.use(express.cookieParser());

var sessionHandler = session({
    secret: 'none',
    rolling: true,
    resave: true,
    saveUninitialized: true
});

app.use(sessionHandler);


/* Global variables. */
var pipelines = {};
var kurentoClient = null;
var playerId = {};
///////////////////////////


//=============================================================
//                Server Startup.
//=============================================================
var asUrl = url.parse(argv.as_uri);
var port = asUrl.port;
var server = app.listen(port, function() {
    console.log('WELCOME!!! Cofa check and Play started');
    console.log('Open ' + url.format(asUrl) + ' with a WebRTC capable browser.');
});

var WebSocket = new ws.Server({
    server: server,
    path: '/cofa-check-and-play'
});

WebSocket.broadcast = function broadcast(data) {
  WebSocket.clients.forEach(function each(client) {
    client.send(data);
  });
};


/* Management of WebSocket messages */
WebSocket.on('connection', function(ws) {
    var sessionId = null;
    var request = ws.upgradeReq;
    var response = {
        writeHead: {}
    }; // black magic here

    sessionHandler(request, response, function(err) {
        sessionId = request.session.id;
        console.log("NODE: Connection received with sessionId " + sessionId);
    });

    ws.on('error', function(error) {
        console.log('NODE: Connection ' + sessionId + ' error');
        stop(sessionId);
    });

    ws.on('close', function() {
        console.log('NODE: Connection ' + sessionId + ' closed');
        stop(sessionId);
    });

    ws.on('threat', function(_message) {
        console.log('NODE: Connection ' + sessionId + ' Event');
    });


    ws.on('message', function(_message) {
        var message = JSON.parse(_message);
        if (message.id != 'start') console.log('NODE: Connection ' + sessionId + ' received message ', message);
        else console.log('NODE: Session started!');

        switch (message.id) {
            case 'start':
                //console.log(message.sdpOffer);
                console.warn("Node: The sdpOffer log is disabled.");
                start(sessionId, message.sdpOffer, function(error, sdpAnswer) {
                    if (error) {
                        return ws.send(JSON.stringify({
                            id: 'error',
                            message: error
                        }));
                    }
                    ws.send(JSON.stringify({
                        id: 'startResponse',
                        sdpAnswer: sdpAnswer
                    }));
                });
                break;

            case 'stop':
                stop(sessionId);
                break;

            case 'pause':
                pause(sessionId);
                break;

            case 'replay':
                rePlay(sessionId);
                break;

//CASE FROM MODULE
            case 'Intrusion detection':
                console.warn('Threat ID:' + message.threat_id);
                console.warn('ID:' + message.id);
                console.warn('Score:' + message.score);
                console.warn('Position X: ' + message.position.x + '  Position Y: ' + message.position.y);
                console.warn('Timestamp:' + message.timestamp);
                rePlay(sessionId);
                WebSocket.broadcast(JSON.stringify({
                    threat_id: message.threat_id,
                    id: message.id,
                    score: message.score,
                    position : { x : message.position.x, y : message.position.y, },
                    timestamp: message.timestamp,
                }));;
                break;


            default:
                ws.send(JSON.stringify({
                    id: 'error',
                    message: 'Invalid message ' + message
                }));
                break;
        }

    });
});


//=============================================================
//                        Functions
//=============================================================

// Recover kurentoClient for the first time.
function getKurentoClient(callback) {
    if (kurentoClient !== null) {
        return callback(null, kurentoClient);
    }
    console.log('NODE: Kurento Client Created')
    kurento(argv.ws_uri, function(error, _kurentoClient) {
        if (error) {
            console.log("NODE: Could not find media server at address " + argv.ws_uri);
            return callback("NODE: Could not find media server at address" + argv.ws_uri + ". Exiting with error " + error);
        }

        kurentoClient = _kurentoClient;
        callback(null, kurentoClient);
    });
}

function start(sessionId, sdpOffer, callback) {
    console.log('NODE: Function start() initializated.')
    if (!sessionId) {
        return callback("NODE: Cannot use undefined sessionId");
    }

    // Check if session is already transmitting
    if (pipelines[sessionId]) {
        return callback("NODE: You already opened this app with this session. Close current session before starting a new one or use another browser.")
    }
    getKurentoClient(function(error, kurentoClient) {
        if (error) return callback(error);
        console.log('NODE: Creating pipeline...')
        kurentoClient.create('MediaPipeline', function(error, pipeline) {
            if (error) return callback(error);
            createMediaElements(pipeline, function(error, player, webRtc) {
                if (error) {
                    pipeline.release();
                    return callback(error);
                }

                connectMediaElements(player, webRtc, function(error) {
                    if (error) {
                        pipeline.release();
                        return callback(error);
                    }
                    webRtc.processOffer(sdpOffer, function(error, sdpAnswer) {
                        if (error) {
                            pipeline.release();
                            return callback(error);
                        }
                        pipelines[sessionId] = pipeline;
                        //playFunction(player);
                        WebSocket.broadcast(JSON.stringify({ id: 'waiting'}));
                        console.warn("Waiting for WM check and event trigger.");
                        playerId[sessionId] = player;
                        return callback(null, sdpAnswer);
                    });
                });
            });
        });
    });

    function createMediaElements(pipeline, callback) {
        pipeline.create('PlayerEndpoint', { uri: argv.file_uri }, function(error, player) {
            console.log('NODE: Player created.')
            if (error) return callback(error);
            pipeline.create('WebRtcEndpoint', function(error, webRtc) {
                if (error) return callback(error);
                console.log('NODE: WebRtc Endpoint created.')
                return callback(null, player, webRtc);
            });
        });
    }

    function connectMediaElements(player, webRtc, callback) {
        player.connect(webRtc, function(error) {
            if (error) return callback(error);
            console.log('NODE: component connected.')
        });
        return callback(null);
    }
}

function rePlay(sessionId) {
    if (pipelines[sessionId]) {
        var player = playerId[sessionId];
        var pipeline = pipelines[sessionId];
        playFunction(player);

    }
}


function playFunction(player) {
    player.play();
    console.log('NODE: Player started.');
    player.on('EndOfStream', function() {
        stop();
        console.warn('NODE: stop() function called. Stopped Player.');
    });
}

function pause(sessionId) {
    if (pipelines[sessionId]) {
        var player = playerId[sessionId];
        player.pause();
        console.log('Player pause.');
    }
}

function stop(sessionId) {
    if (pipelines[sessionId]) {
        var pipeline = pipelines[sessionId];
        var player = playerId[sessionId];
        player.stop();
        console.log('Player stopped.');
        pipeline.release();
        delete pipelines[sessionId];
    }
}

app.use(express.static(path.join(__dirname, 'static')));